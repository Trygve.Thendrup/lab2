package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	List<FridgeItem> fridgeContainsList = new ArrayList<FridgeItem>();

	@Override
	public int nItemsInFridge() {
		return (fridgeContainsList).size();
	}

	@Override
	public int totalSize() {
		return 20;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if(nItemsInFridge()<totalSize()){
			fridgeContainsList.add(item);
			return  true;
		}
		else {
			return false;
		}
	}

	@Override
	public void takeOut(FridgeItem item) {
		if(fridgeContainsList.contains(item)) {
			fridgeContainsList.remove(item);
		}
		else {
			throw new NoSuchElementException();
		}

	}

	@Override
	public void emptyFridge() {
		fridgeContainsList.clear();

	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredList = new ArrayList<FridgeItem>();
		for( int i = 0; i<nItemsInFridge(); i++) {
			if(fridgeContainsList.get(i).hasExpired()) {
				expiredList.add(fridgeContainsList.get(i));
				
			}
		}
		for(FridgeItem item : expiredList ) {
			if(fridgeContainsList.contains(item)) {
				takeOut(item);
			}
		}
		
		
		return expiredList;
	}


}
